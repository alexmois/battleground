<?php
/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 06:55
 */

class CellGenerator extends BaseObject
{
    const GRASS = 'Grass';
    const HILL = 'Hill';
    const LAKE = 'Lake';

    protected $types;
    protected $randomizator;

    protected $grass;
    protected $hill;
    protected $lake;

    public function __construct($config)
    {
        parent::__construct($config);

        $this->randomFiller(self::GRASS, $this->grass);
        $this->randomFiller(self::HILL, $this->hill);
        $this->randomFiller(self::LAKE, $this->lake);
    }

    /**
     * Fill array for battleground cells generating
     *
     * @param $type
     * @param $percentage
     * @return bool
     */
    protected function randomFiller($type, $percentage)
    {
        for ($i = 0; $i <= $percentage; $i++) {
            $this->randomizator[] = $type;
        }
        return true;
    }

    /**
     * Return cells by type
     *
     * @param $cellType
     * @param $map
     * @param $x
     * @param $y
     * @return Grass|Hill|Lake
     */
    public function getCell($cellType, $map, $x, $y)
    {
        switch ($cellType) {
            case self::HILL:
                return new Hill([
                    'map' => $map,
                    'position' => [
                        'x' => $x,
                        'y' => $y,
                    ]
                ]);
                break;
            case self::LAKE:
                return new Lake([
                    'map' => $map,
                    'position' => [
                        'x' => $x,
                        'y' => $y,
                    ]
                ]);
                break;
            default:
                return new Grass([
                    'map' => $map,
                    'position' => [
                        'x' => $x,
                        'y' => $y,
                    ]
                ]);
        }
    }

    /**
     * Getting random type of cell
     *
     * @param $map
     * @param $x
     * @param $y
     * @return Grass|Hill|Lake
     */
    public function getRandom($map, $x, $y)
    {
        return self::getCell($this->randomizator[random_int(0, (count($this->randomizator) - 1))], $map, $x, $y);
    }
}