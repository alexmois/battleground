<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 06:51
 */

abstract class Flat extends BaseObject implements Cell, Coordinatable, Identifiable
{
    protected $typeSymbol;
    protected $used = false;
    protected $map;

    protected $position;

    abstract function updateTypeSymbol();

    public function __construct($config)
    {
        $this->setPosition($config['position']);
        unset($config['position']);

        parent::__construct($config);
        $this->updateTypeSymbol();
    }

    public function markUsed($object)
    {
        $this->used = $object;
    }

    public function unMarkUsed()
    {
        $this->used = false;
    }

    public function setTypeSymbol($typeSymbol)
    {
        $this->typeSymbol = $typeSymbol;
    }

    public function getTypeSymbol()
    {
        return $this->typeSymbol;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = new Position($position);
    }

    public function getUsed()
    {
        return $this->used;
    }

    public function setUsed($used)
    {
        $this->used = $used;
    }

    public function getMap()
    {
        return $this->map;
    }

    public function setMap($map)
    {
        $this->map = $map;
    }



}