<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 06:53
 */

require_once 'Flat.php';

class Lake extends Flat
{
    public function updateTypeSymbol()
    {
        $this->typeSymbol = 'o';
    }

}