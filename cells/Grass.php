<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 06:53
 */
class Grass extends Flat
{
    public function updateTypeSymbol()
    {
        $this->typeSymbol = '_';
    }
}