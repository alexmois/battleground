<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 07:40
 */


class Position extends BaseObject implements Coordinatable
{
    protected $x;
    protected $y;

    public function getX()
    {
        return $this->x;
    }

    public function setX($x)
    {
        $this->x = $x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function setY($y)
    {
        $this->y = $y;
    }
}