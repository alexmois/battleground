<?php
require_once 'cells/CellGenerator.php';
/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 06:50
 */

class Map extends BaseObject
{
    protected $n; // x size
    protected $m; // y size

    protected $grass; // percentage of grass cells
    protected $hill; // percentage of grass hill
    protected $lake; // percentage of grass lake

    protected $ground;

    public function __construct(array $config)
    {
        parent::__construct($config);
        // Call cell generator
        $cellGenerator = new CellGenerator([
            'grass' => $this->grass,
            'hill' => $this->hill,
            'lake' => $this->lake,
        ]);

        // Fill map at random cells
        for ($i = 1; $i <= $this->m; $i++) {
            for ($j = 1; $j <= $this->n; $j++) {
                $this->ground[$j][$i] = $cellGenerator->getRandom($this, $j, $i);;
            }
        }
    }


    /**
     * Method for map rendering
     *
     * @param bool $withMarks
     */
    public function renderGround($withMarks = false)
    {
        for ($i = 1; $i <= $this->m; $i++) {
            for ($j = 1; $j <= $this->n; $j++) {
                $cell = $this->ground[$j][$i];

                if ($withMarks == true) {
                    if ($cell->used != false) {
                        echo $cell->used->typeSymbol;
                    } else {
                        echo $cell->typeSymbol;
                    }
                } else {
                    echo $cell->typeSymbol;
                }
            }
            echo PHP_EOL;
        }
    }


    /**
     * Checking free cell for random unit placing
     *
     * @param $object
     * @param $x
     * @param $y
     * @param $direction
     * @return array|bool
     * @throws Exception
     */
    public function getFreeCell($object, $x, $y, $direction)
    {

        $cell = [];
        switch ($object) {
            case $object instanceof Building:

                $cell = $this->getFreeInstanceCell(Grass::class, $x, $y, $direction);
                break;
            case $object instanceof Plane:

                $cell = $this->getFreeInstanceCell(Flat::class, $x, $y, $direction);
                break;
            case $object instanceof Tank:

                $cell = $this->getFreeInstanceCell(Grass::class, $x, $y, $direction);
                break;
            case $object instanceof RifleMan:

                $cell = $this->getFreeInstanceCell(Grass::class, $x, $y,  $direction);
                break;
            default:
                throw new \Exception('Unknown object type');
        }

        return $cell;
    }

    /**
     * Helpful method for cycle instance checking
     *
     * @param $instance
     * @param int $x
     * @param int $y
     * @param $direction
     * @return bool
     */
    protected function getFreeInstanceCell($instance, $x = 1, $y = 1, $direction)
    {

        for ($i = $x; $i <= $this->m; $i++) {
            for ($j = $y; $j <= $this->n; $j++) {
                if ($this->ground[$j][$i] instanceof $instance && $this->ground[$j][$i]->used == false) {
                    return $this->ground[$j][$i];
                }

            }
        }
        return false;
    }

    /**
     * Rules of acceptable cells for move and placing
     *
     * @param $unit
     * @param $x
     * @param $y
     * @return bool
     */
    public function checkMove($unit, $x, $y)
    {
        $move = false;
        $cell = $this->ground[$x][$y];

        if ($unit instanceof Plane) {
            $move = true;
        }

        if ($unit instanceof Tank && $cell instanceof Grass) {
            $move = true;
        }

        if ($this instanceof Human && $cell instanceof Grass) {
            $move = true;
        }

        return $move;
    }

    public function getN()
    {
        return $this->n;
    }

    public function setN($n)
    {
        $this->n = $n;
    }

    public function getM()
    {
        return $this->m;
    }

    public function setM($m)
    {
        $this->m = $m;
    }

    public function getGround()
    {
        return $this->ground;
    }

    public function setGround($ground)
    {
        $this->ground = $ground;
    }

    /**
     * Update battleground matrix
     *
     * @param $cell
     * @param $x
     * @param $y
     */
    public function updateGround($cell, $x, $y)
    {
        $this->ground[$x][$y] = $cell;
    }


}