<?php
/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 07:10
 */


require_once 'objects/BaseObject.php';
require_once 'objects/Coordinatable.php';
require_once 'objects/Identifiable.php';
require_once 'objects/Position.php';
require_once 'objects/Movable.php';
require_once 'objects/SideA.php';
require_once 'objects/SideB.php';

require_once 'cells/Cell.php';
require_once 'cells/Flat.php';
require_once 'cells/Hill.php';
require_once 'cells/Grass.php';
require_once 'cells/Lake.php';

require_once 'buildings/Building.php';
require_once 'buildings/Construction.php';
require_once 'buildings/WarBase.php';

require_once 'units/Unit.php';
require_once 'units/MovableUnit.php';
require_once 'units/Tank.php';
require_once 'units/Plane.php';
require_once 'units/Human.php';
require_once 'units/tanks/T90.php';
require_once 'units/tanks/Abrams.php';
require_once 'units/planes/Mig29.php';
require_once 'units/planes/Phantom.php';
require_once 'units/solders/RifleMan.php';
