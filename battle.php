<?php
/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 07:10
 */


require_once 'bootstrap.php';
require_once 'Command.php';
require_once 'Map.php';


echo 'START GENERATE MAP'.PHP_EOL;
$map = new Map([
    'n' => 30, //cells
    'm' => 10, //cells

    'grass' => 80, //%
    'hill' => 10, //%
    'lake' => 10, //%
]);


echo 'START FILL COMMAND A'.PHP_EOL;
$commandA = new Command([
    'map' => $map, //in real life its may be as Singleton
    'side' => SideA::class,
    'base' => new WarBase([
        'health' => 100
    ]),
    'plane' => new Phantom([
        'health' => 75,
        'power' => 110
    ]),
    'rifle' => new RifleMan([
        'health' => 10,
        'power' => 5
    ]),
    'tank' => new Abrams([
        'health' => 50,
        'power' => 10
    ])

]);

echo 'START FILL COMMAND B'.PHP_EOL;
$commandB = new Command([
    'map' => $map,
    'side' => SideB::class,
    'base' => new WarBase([
        'health' => 100
    ]),
    'plane' => new Mig29([
        'health' => 80,
        'power' => 100
    ]),
    'rifle' => new RifleMan([
        'health' => 10,
        'power' => 5
    ]),
    'tank' => new T90([
        'health' => 50,
        'power' => 10
    ])
]);


echo 'RENDER OUR BEAUTIFUL BATTLEGROUND'.PHP_EOL;
$map->renderGround(true);

echo PHP_EOL;
echo 'Plane A position now: X = '.$commandA->plane->position->x.'; Y = '.$commandA->plane->position->y.PHP_EOL;
echo 'Move plane A to little bit to down on map'.PHP_EOL;
$commandA->plane->moveDown();
$commandA->plane->moveDown();
$commandA->plane->moveDown();
echo 'Plane A position after move: X = '.$commandA->plane->position->x.'; Y = '.$commandA->plane->position->y.PHP_EOL;

echo PHP_EOL;
echo 'Start battle beetwen Plane A and Plane B.'.PHP_EOL;
echo 'Plane B health: '.$commandB->plane->health.PHP_EOL;
echo 'Plane A attack Plane B'.PHP_EOL;
echo 'Strike > ';
$commandA->plane->fire($commandB->plane);
echo 'Plane B health: '.$commandB->plane->health.PHP_EOL;
echo 'Killed plane B try to attack Plane A'.PHP_EOL;
$commandB->plane->fire($commandA->plane);
echo PHP_EOL;
echo 'Okay...show must go on.'.PHP_EOL;
echo 'RifleMan B try to attack Plane A'.PHP_EOL;
$commandB->rifle->fire($commandA->plane);
echo 'RifleMan B try to attack RifleMan A'.PHP_EOL;
echo 'Strike > ';
$commandB->rifle->fire($commandA->rifle);
echo 'Strike > ';
$commandB->rifle->fire($commandA->rifle);

echo PHP_EOL.PHP_EOL;

echo 'That`s all kids...'.PHP_EOL;
