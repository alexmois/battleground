<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 08:51
 */
class RifleMan extends Human
{
    public function updateTypeSymbol()
    {
        $this->typeSymbol = 'R';
    }
}