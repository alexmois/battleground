<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 08:55
 */
abstract class MovableUnit extends BaseObject implements Unit, Coordinatable, Identifiable, Movable
{

    protected $typeSymbol;
    protected $position;
    protected $health;
    protected $power;
    protected $cell;

    abstract function updateTypeSymbol();

    public function __construct($config)
    {
        parent::__construct($config);
        $this->updateTypeSymbol();
    }

    public function setTypeSymbol($typeSymbol)
    {
        $this->typeSymbol = $typeSymbol;
    }

    public function getTypeSymbol()
    {
        return $this->typeSymbol;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = new Position($position);
    }

    public function moveUp()
    {
        return $this->move($this->position->x, $this->position->y--);
    }

    public function moveDown()
    {
        return $this->move($this->position->x, $this->position->y++);
    }

    public function moveLeft()
    {
        return $this->move($this->position->x--, $this->position->y);
    }

    public function moveRight()
    {
        return $this->move($this->position->x++, $this->position->y);
    }

    protected function move($x, $y)
    {
        if ($this->health == 0) {
            echo 'This unit can`t move, because it`s killed.'.PHP_EOL;
            return false;
        }

        if ($this->cell->map->checkMove($this, $x, $y)) {
            $this->cell->unMarkUsed($this);
            $this->cell->map->updateGround($this->cell, $x, $y);
            $this->cell->markUsed($this);
            return true;
        }
        return false;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }

    public function getPower()
    {
        return $this->power;
    }

    public function setPower($power)
    {
        $this->power = $power;
    }

    public function getCell()
    {
        return $this->cell;
    }

    public function setCell($cell)
    {
        $this->cell = $cell;
    }

    public function fire($unit)
    {
        if ($this->health == 0) {
            echo 'This unit can`t attack another unit, because it`s killed.'.PHP_EOL;
            return false;
        }

        /***
         *  P - Plane
         *  T - Tank
         *  H - Human
         *
         *      P T H
         *     ------
         *  P | 1 1 1
         *  T | 0 1 1
         *  H | 0 0 1
         *
         * If siez of unit types will be more than 4 (4x4 matrix), logical must be programming at multiarray.
         */

        $fire = false;

        if ($this instanceof Plane && ($unit instanceof Human || $unit instanceof Tank || $unit instanceof Plane)) {
            $fire = true;
        }

        if ($this instanceof Tank && ($unit instanceof Human || $unit instanceof Tank)) {
            $fire = true;
        }

        if ($this instanceof Human && $unit instanceof Human) {
            $fire = true;
        }

        $health = $unit->health - $this->power;
        if ($health < 0) {
            $health = 0;
        }
        $unit->health = $health;

        if ($fire == true) {
            if ($health <= 0) {
                echo 'Totally killed.'.PHP_EOL;
            } else {
                echo '-'.$this->power.' HP (Current HP '.$unit->health.')'.PHP_EOL;
            }

        } else {
            echo 'This unit can`t attack.'.PHP_EOL;
        }
        return $fire;
    }

}