<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 08:52
 */
class Phantom extends Plane
{
    public function updateTypeSymbol()
    {
        $this->typeSymbol = 'P';
    }

}