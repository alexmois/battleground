<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 08:07
 */
class Command extends BaseObject
{

    protected $map;
    protected $side;
    protected $base;
    protected $tank;
    protected $rifle;
    protected $plane;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->putOnMap($this->base);
        $this->putOnMap($this->plane);
        $this->putOnMap($this->rifle);
        $this->putOnMap($this->tank);
    }


    /**
     * Place movable units to battleground
     *
     * @param $object
     * @throws Exception
     */
    protected function putOnMap($object)
    {
        switch ($this->side) {
            // Check command for place base and units at difference part of battleground
            case SideA::class:
                $direction = 1;
                $x = (int)($this->map->m / 4);
                $y = (int)($this->map->n / 4);
                break;
            case SideB::class:
                $direction = -1;
                $x = (int)($this->map->m / 2);
                $y = (int)($this->map->n / 2);

                break;
            default:
                throw new Exception('Unknown side');
        }

        // Checking random cell
        $object->cell = $this->map->getFreeCell($object, $x, $y, $direction);

        // Accept cell x/y for unit position
        $object->position = clone($object->cell->position);

        // Mark this cell as used by unit
        $object->cell->markUsed($object);
    }

    public function getSide()
    {
        return $this->side;
    }

    public function setSide($side)
    {
        $this->side = $side;
    }

    public function getBase()
    {
        return $this->base;
    }

    public function setBase($base)
    {
        $this->base = $base;
    }

    public function getTank()
    {
        return $this->tank;
    }

    public function setTank($tank)
    {
        $this->tank = $tank;
    }

    public function getRifle()
    {
        return $this->rifle;
    }

    public function setRifle($rifle)
    {
        $this->rifle = $rifle;
    }

    public function getPlane()
    {
        return $this->plane;
    }

    public function setPlane($plane)
    {
        $this->plane = $plane;
    }

    public function getMap()
    {
        return $this->map;
    }

    public function setMap($map)
    {
        $this->map = $map;
    }
}