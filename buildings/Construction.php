<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 07:37
 */

abstract class Construction extends BaseObject implements Building, Coordinatable, Identifiable
{
    protected $health;
    protected $position;
    protected $typeSymbol;
    protected $cell;

    abstract function updateTypeSymbol();

    public function __construct($config)
    {
        parent::__construct($config);
        $this->updateTypeSymbol();
    }

    public function setTypeSymbol($typeSymbol)
    {
        $this->typeSymbol = $typeSymbol;
    }

    public function getTypeSymbol()
    {
        return $this->typeSymbol;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = new Position($position);
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }

    public function getCell()
    {
        return $this->cell;
    }

    public function setCell($cell)
    {
        $this->cell = $cell;
    }



}