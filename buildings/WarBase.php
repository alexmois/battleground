<?php

/**
 * Author: Alexander Moiseykin
 * E-mail: master@cifr.us
 * Date: 28.06.2018
 * Time: 07:37
 */
class WarBase extends Construction
{

    public function updateTypeSymbol()
    {
        $this->typeSymbol = 'B';
    }

}